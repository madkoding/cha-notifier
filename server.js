const notifier = require("node-notifier");
const url = "https://api.orionx.io/graphql";
const coinId = getCoinId("cha");
const https = require("https");
const path = require("path");
const minutesToCheck = 5;

function getCoinId(coin) {
    const coins = {
        bitcoin: "BTCCLP",
        btc: "BTCCLP",
        eth: "ETHCLP",
        ethereum: "ETHCLP",
        litecoin: "LTCCLP",
        ltc: "LTCCLP",
        dash: "DASHCLP",
        "bitcoin-cash": "BCHCLP",
        bch: "BCHCLP",
        cha: "CHACLP",
        chaucha: "CHACLP"
    };
    return coins[coin];
}

function getOrionxHistory(coinId) {
    const query = `{
      marketTradeHistory(marketCode: "${coinId}") {
        _id
        amount
        price
        totalCost
        date
      }
    }`;

    return new Promise((resolve, reject) => {
        let body = "";
        https
            .get(`${url}?query=${query}`, resp => {
                resp.on("data", chunk => {
                    body += chunk;
                });
                resp.on("end", () => {
                    try {
                        const json = JSON.parse(body);
                        if (json.data && json.data.marketTradeHistory && json.data.marketTradeHistory.length > 0) {
                            let history = json.data.marketTradeHistory;
                            let actual = history[0];
                            let previous = history[1];
                            let average =
                                history
                                    .map(his => {
                                        return his.price;
                                    })
                                    .reduce((a, b) => {
                                        return a + b;
                                    }, 0) / history.length;
                            resolve({
                                price: actual.price,
                                status: {
                                    up: actual.price > previous.price,
                                    down: actual.price < previous.price,
                                    eq: actual.price == previous.price
                                },
                                average: average
                            });
                        } else {
                            reject();
                        }
                    } catch (e) {
                        reject();
                    }
                });
            })
            .on("error", err => {
                console.log("Error: " + err.message);
            });
    });
}

function checkStatus(result) {
    if (result.status.eq) return "<font color='#00F'>se mantiene</font>";
    if (result.status.up) return "<font color='#0F0'>subió</font>";
    if (result.status.down) return "<font color='#F00'>bajó</font>";
}

function check() {
    getOrionxHistory(coinId)
        .then(lastResult => {
            notifier.notify({
                title: "Precio de CHA <b>" + checkStatus(lastResult) + "</b>",
                message:
                    "Precio actual: <b>$" +
                    lastResult.price +
                    "</b> CLP" +
                    "<br/>" +
                    "Precio promedio: <b>$" +
                    parseInt(lastResult.average) +
                    "</b> CLP",
                icon: path.join(__dirname, "chaucha.png")
            });
        })
        .catch(err => {
            console.log(err);
            notifier.notify(`No se puso consultar por CHA.`);
        });
}

check(); // Check at startup ;)
setInterval(function() {
    check();
}, minutesToCheck * 60 * 1000); // Check every 2 min
